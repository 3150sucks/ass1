#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>
#include <glob.h>

#define BUFMAX 255
#define SHELLNAME "3150 shell"
#define STRINGIS(TOK,STR) (!strcmp(token[TOK],STR))

char token[130][BUFMAX];
int count, pipeC;

typedef struct job{
    int status;
    pid_t pid;
    char *cmd;
} job;

job *current = NULL;
job bgrd[50];
int NoOfBgrd = 0;

void sig_handler(int signo){
    return;
}

void SuspendJobs(int signo){
    //bgrd = malloc(sizeof(job)*NoOfBgrd+1);
    bgrd[NoOfBgrd++] = *current;
    printf("\n");
    //printf("[%d] %s", NoOfBgrd, current->cmd);
    kill(current->pid, SIGINT);
}

void jobs(){
    int i = 0;
    if(NoOfBgrd == 0){
	printf("No suspended jobs\n");
    }else{
        for(i = 0; i < NoOfBgrd; i++){
	    printf("[%d] %s", i+1, bgrd[i].cmd);
        }
    }
}

void fgs(char *jjid){
    int jid = atoi(jjid);
    int i;
    int status;
    if (jid > NoOfBgrd || jid <= 0) {printf("fg: no such job\n");return;}
    printf("Job wake up: %s", bgrd[jid-1].cmd);
    //waitpid(bgrd[jid-1].pid, &status, WUNTRACED);
    kill(bgrd[jid-1].pid, SIGCONT);
    NoOfBgrd--;
    for(i = jid-1; i < 49; i++){
	bgrd[i] = bgrd[i+1];
    }
}

int grammarCheck()
{
	int i, cpipe = 0;
	char cmKeys[] = " 	><|*!`'\"";
	char arKeys[] = " 	><|!`'\"";
	for (i = 0; i < count; i++){
		if (i == 0 || !strcmp(token[i-1], "|")){
			if (strcspn(token[i], cmKeys) < strlen(token[i]))
				return 0;
		} else if (!(STRINGIS(i-1,"cd") || STRINGIS(i-1,"exit") || STRINGIS(i-1,"fg") || STRINGIS(i-1,"jobs")) && STRINGIS(i,"|")){
			if (i == 0 || i == count-1)
				return 0;
			if (++cpipe > 2) return 0;
		} else {
			if (strcspn(token[i], arKeys) < strlen(token[i]))
				return 0;
		}
	}
	pipeC = cpipe;
	return 1;
}

char* toktype(int index, int state)
{
	if (!strcmp(token[index], "|")){
		return "Pipe";
	} else if (index == 0 || !strcmp(token[index-1], "|")){
		if (state){
			return "Built-in Command";
		} else return "Command Name";
	} else return "Argument";
}

void printToken(int state)
{
//	int i;
//	for (i = 0; i < count; i++)
//		printf("Token %d: \"%s\" (%s)\n", i+1, token[i], toktype(i,state));
}

void callcd()
{
	if (count > 2){
		printf("cd: wrong number of arguments\n");
	} else if (chdir(token[1])){
		printf("%s: cannot change directory\n", token[1]);
	} else printToken(1);
}

void callexit()
{
	if (count > 1){
		printf("exit: wrong number of arguments\n");
	} else if(NoOfBgrd >0){
		printf("There is at least one suspended job\n");
	}
	else printToken(1);
}

void callfg()
{
	if (count > 2){
		printf("fg: wrong number of arguments\n");
	} else {
		printToken(1);
		//PHASE 2
		fgs(token[1]);
	}
}

void calljobs()
{
	if (count > 1){
		printf("jobs: wrong number of arguments\n");
	} else {
		printToken(1);
		//PHASE 2
		jobs();
	}
}

void callFunc()
{
	int i;
	//printf("Pipe: %d\n", pipeC);
	if (!strcmp(token[0], "cd")){
		callcd();
	} else if (!strcmp(token[0], "exit")){
		callexit();
	} else if (!strcmp(token[0], "fg")){
		callfg();
	} else if (!strcmp(token[0], "jobs")){
		calljobs();
	} else {
		printToken(0);
		glob_t globbuf[3];
		int sec = -1;
		int a[3];
		char cmd[3][BUFMAX];
		char *para[3][100];
		for (i = 0; i < count; i++){
			sec++;
			if (i == 0 || !strcmp(token[i-1], "|")){
				//PHASE 2 run command
				a[sec]=0;
				globbuf[sec].gl_offs = 1;
				while (strcmp(token[i], "|") && i < count){
					//para[a++]=token[i++];
					if (a[sec] == 0){
						strcpy(cmd[sec],token[i++]);
					} else if (a[sec] == 1){
						if (glob(token[i++], GLOB_DOOFFS | GLOB_NOCHECK, NULL, &globbuf[sec])){
							//ERROR HANDLE???
						}
					} else {
						if (glob(token[i++], GLOB_DOOFFS | GLOB_NOCHECK | GLOB_APPEND, NULL, &globbuf[sec])){
							//ERROR HANDLE???
						}
					}
					a[sec]++;
				}
				if (a[sec]==1){
					para[sec][0] = cmd[sec];
					para[sec][1] = NULL;
				} else {
					globbuf[sec].gl_pathv[0] = cmd[sec];
				}
				if(pipeC) continue;
				//para[a] = NULL;
				int freturn = fork();
					//ERROR HANDLE???
				if (freturn == 0){
					setenv("PATH", "/bin:/usr/bin:.", 1);
					if (a[sec]==1){
						execvp(*para[sec], para[sec]);
						//execvp(cmd, globbuf[0].gl_pathv);
						if (errno == ENOENT){
							printf("%s: command not found\n", *para[sec]);
							//printf("%s: command not found\n", *globbuf[0].gl_pathv);
						} else {
							printf("%s: unknown error\n", *para[sec]);
							//printf("%s: unknown error\n", *globbuf[0].gl_pathv);
						}
					} else {
						//execvp(*para, para);
						execvp(cmd[sec], globbuf[sec].gl_pathv);
						if (errno == ENOENT){
							//printf("%s: command not found\n", *para);
							printf("%s: command not found\n", *globbuf[sec].gl_pathv);
						} else {
							//printf("%s: unknown error\n", *para);
							printf("%s: unknown error\n", *globbuf[sec].gl_pathv);
						}
					}
					exit(-1);
				} else {
					waitpid(freturn, NULL, 0);
					//ERROR HANDLE???
				}

			}
		}
		if (pipeC){
			//pipe start here
			int fd[2], fd2[2];
			int pid1, pid2, pid3;
			pipe(fd);
//			printf("%d\t%d\t%d", a[0], a[1], a[2]);
			if (pid1 = fork()){
				if (pipeC == 2) pipe(fd2);
				if (pid2 = fork()){
					if (pipeC == 1){
						close(fd[0]);
						close(fd[1]);
						waitpid(pid1, NULL, 0);
						waitpid(pid2, NULL, 0);
					} else {
						if (pid3 = fork()){
							close(fd[0]);
							close(fd[1]);
							close(fd2[0]);
							close(fd2[1]);
							waitpid(pid1, NULL, 0);
							waitpid(pid2, NULL, 0);
							waitpid(pid3, NULL, 0);
						} else {
							dup2(fd2[0], 0);
							close(fd[0]);
							close(fd[1]);
							close(fd2[0]);
							close(fd2[1]);
							sec=2;
							//execvp
//							printf("33333\n");
							setenv("PATH", "/bin:/usr/bin:.", 1);
							if (a[sec]==1){
								execvp(*para[sec], para[sec]);
								if (errno == ENOENT){
									printf("%s: command not found\n", *para[sec]);
								} else {
									printf("%s: unknown error\n", *para[sec]);
								}
							} else {
								execvp(cmd[sec], globbuf[sec].gl_pathv);
								if (errno == ENOENT){
									printf("%s: command not found\n", *globbuf[sec].gl_pathv);
								} else {
									printf("%s: unknown error\n", *globbuf[sec].gl_pathv);
								}
							}
							exit(-1);
						}
					}
				} else {
					dup2(fd[0], 0);
					if (pipeC == 2){
						//dup2(fd[1],1);
						//dup2(fd2[0],0);
						dup2(fd2[1],1);
					}
					close(fd[0]);
					close(fd[1]);
				if(pipeC == 2){
					close(fd2[0]);
					close(fd2[1]);}
					sec=1;
					//execvp
//					printf("22222\n");
					setenv("PATH", "/bin:/usr/bin:.", 1);
					if (a[sec]==1){
						execvp(*para[sec], para[sec]);
						if (errno == ENOENT){
							printf("%s: command not found\n", *para[sec]);
						} else {
							printf("%s: unknown error\n", *para[sec]);
						}
					} else {
						execvp(cmd[sec], globbuf[sec].gl_pathv);
						if (errno == ENOENT){
							printf("%s: command not found\n", *globbuf[sec].gl_pathv);
						} else {
							printf("%s: unknown error\n", *globbuf[sec].gl_pathv);
						}
					}
					exit(-1);
				}
			} else {
//				printf("11111\n");
				dup2(fd[1], 1);
				close(fd[0]);
				close(fd[1]);
				if(pipeC == 2){
				close(fd2[0]);
				close(fd2[1]);}
				sec=0;
				//execvp
					setenv("PATH", "/bin:/usr/bin:.", 1);
					if (a[sec]==1){
						execvp(*para[sec], para[sec]);
						if (errno == ENOENT){
							printf("%s: command not found\n", *para[sec]);
						} else {
							printf("%s: unknown error\n", *para[sec]);
						}
					} else {
						execvp(cmd[sec], globbuf[sec].gl_pathv);
						if (errno == ENOENT){
							printf("%s: command not found\n", *globbuf[sec].gl_pathv);
						} else {
							printf("%s: unknown error\n", *globbuf[sec].gl_pathv);
						}
					}
					exit(-1);
			}
		}
	}
}

int main()
{
	char cwd[1024], buf[BUFMAX];
	char *tok;

	//signal(SIGSTOP, sig_handler);
	signal(SIGINT, sig_handler);
	signal(SIGTERM, sig_handler);
	signal(SIGQUIT, sig_handler);
	signal(SIGTSTP, SuspendJobs);

	getcwd(cwd, sizeof(cwd));
	printf("["SHELLNAME":%s]$ ", cwd);

	while (fgets(buf, BUFMAX, stdin))
	{
		current = malloc(sizeof(job));
		current->cmd = malloc(sizeof(buf));
		strcpy(current->cmd, buf);
		count = 0;
		tok = strtok(buf, " \n");
		while (tok != NULL)
		{
			strcpy(token[count++],tok);

			tok = strtok(NULL , " \n");
		}

		if (grammarCheck())
		{
			callFunc();
		} else {
			printf("Error: invalid input command line\n");
		}

		if (!strcmp(buf, "exit") && count == 1 && NoOfBgrd == 0) {
			printf("[Shell Terminated]");
			break;
		}

		getcwd(cwd, sizeof(cwd));
		printf("["SHELLNAME":%s]$ ", cwd);
	}
	printf("\n");
	return 0;
}

