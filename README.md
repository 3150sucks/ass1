# README #

### Progress ###

* Milestone 1 (Execute Command)
    * DONE

* Milestone 2 (Pipe)

* Milestone 3 (Signal)
    * DONE

* Milestone 4 (fg & jobs)


### Reference ###
* [Specification](http://appsrv.cse.cuhk.edu.hk/~csci3150/downloads/shell.pdf)


***
nkyeung3, htlam3 @ CSCI3150 2014Fall